from collections import defaultdict

"""
Найти наименьшее натуральное число, неединственным образом представимое в виде суммы кубов двух натуральных чисел.
"""


def seventeenth_exercise(maximum):
    cubes = defaultdict(list)
    for i in range(1, maximum):
        for j in range(1, i + 1):
            s = i ** 3 + j ** 3
            cubes[s].append((i, j))

    for key, value in cubes.items():
        if len(value) > 1:
            print(key, value)


seventeenth_exercise(int(input("Введите число: ")))
