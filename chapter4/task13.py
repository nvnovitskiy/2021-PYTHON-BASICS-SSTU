import random

"""
Первые k элементов числовой последовательности равны f1,
f2, …, fk, а каждое следующее значение вычисляется по рекуррентной
формуле f_n = sum(j=1, k) a_j * f_{n-j}, где n>k. Задать в программе начальные значения
последовательности, коэффициенты a1, a2,…, ak и значение n и вычислить fn. 
"""
s = 0
n, k = map(int, input("Введите значение n: ").split())
f = [random.uniform(1, 50) for _ in range(n - 1)]
a = [random.uniform(1, 50) for _ in range(n - 1)]

if n < k: 
    print("n должно быть больше k")
else:
    for j in range(0, k):
        s += a[j] * f[n - j - 2]

print(f"Значение f_n: {s:.3f}")


