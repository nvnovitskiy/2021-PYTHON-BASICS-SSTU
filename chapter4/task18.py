import itertools

"""
Дано N натуральных чисел. Требуется выписать в строчку
все эти числа так, чтобы образовалось максимально возможное число.
Например, из чисел 7, 24, 101, 35 получится 73524101.
"""


def eighteenth_exercise(number):
    max_num = 0
    for i in itertools.permutations(number):
        num = int("".join(i))
        if num > max_num:
            max_num = num
    return max_num


number = eighteenth_exercise(input("Введите число: ").split(", "))
print(f"Максимальное возможное число: {number}")
