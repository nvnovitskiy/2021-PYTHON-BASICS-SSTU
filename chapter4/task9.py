from random import uniform

"""
Заполнить одномерный массив A длиной N случайными вещественными числами от 0 до 10 (значение N вводится с клавиатуры). 
Сформировать новый массив B, в котором значение B[i] равно количеству элементов массива A, удовлетворяющих неравенству
i <= a < (i+1) для i от 0 до 9. Массив A «в столбик», а массив B – «в строку» вывести на экран.
"""

quantity = int(input("Введите количество элементов массива: "))
first_array = [uniform(0, 10) for _ in range(quantity)]
second_array = [len(list(filter(lambda a: i <= a < (i + 1), first_array))) for i in range(len(first_array))]

print(*first_array, sep="\n")
print(second_array)
