"""
Ввести строку, содержащую несколько слов. 
Определить, какие слова из введенной строки являются палиндромами. 
Вывести список слов-палиндромов.
"""


def second_exercise(string):
    palindromes = []
    x = string.split()
    for i in range(len(x)):
        if x[i] == x[i][::-1]:
            palindromes.append(x[i])
    return palindromes


print(second_exercise(input("Введите строку: ")))
