"""
Дан список подсписков вида [a, b], где a есть левая, а b –
правая граница отрезка на оси Ox. Определить, имеют ли все отрезки
общую точку.
"""


def fifteenth_exercise(points):
    first_section = range(points[0][0], points[0][1] + 1)
    second_section = range(points[1][0], points[1][1] + 1)
    condition = [a for a in first_section if a in second_section]
    if len(condition) == 0:
        print("Пустое множество")
    else:
        print(*sorted({condition[0], condition[-1]}))


fifteenth_exercise([[4, 10], [6, 12]])
