"""
Ввести n и создать «треугольный» двумерный массив вида
1
4 1
9 4 1
…
n^2 (n-1)^2 … 1
Вывести на экран в двумерном виде сам список и сумму всех его элементов.
"""

number = int(input("Введите число: "))
counter = 0

for x in range(1, number + 1):
    array = []
    while x > 0:
        array.append(x ** 2)
        counter += x ** 2
        x -= 1
    print(*array)
print(f"Сумма элементов массива: {counter}")
