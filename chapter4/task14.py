"""
Дан список A подсписков вида [a, b], где a есть левая, а b –
правая граница отрезка на оси Ox. Определить новый отрезок, 
который покрывается максимальным числом отрезков из исходного набора и само это число.
Пример. A=[[1,3],[1,7],[0,3],[3,7],[2,5]], тогда отрезок [2,3] покрывается четырьмя отрезками из исходного набора; это отрезки 
[1,3], [1,7], [0,3] и [2,5]. Заметим, что результирующий отрезок может не совпадать ни с одним отрезком исходного набора.
"""


def fourteenth_exercise(first_list):
    second_list = [list(range(elem[0], elem[1] + 1)) for elem in first_list]
    third_list = []
    for x in range(10):
        counter = 0
        for y in range(len(first_list)):
            if x in second_list[y]:
                counter += 1
        third_list.append(counter)
    if third_list[third_list.index(max(third_list)) - 1] >= third_list[third_list.index((max(third_list))) + 1]:
        return [third_list.index(max(third_list)) - 1, third_list.index(max(third_list))]
    return [third_list.index(max(third_list)), third_list.index(max(third_list)) + 1]


first_list = [[1, 3], [1, 7], [0, 3], [3, 7], [2, 5]]
print(fourteenth_exercise(first_list))
