"""
Ввести исходное слово и строку, содержащую несколько слов. 
Определить, какие слова из введенной строки являются анаграммами исходного слова. 
Вывести исходное слово и список слованаграмм.
"""


def first_exercise(string, word):
    anagrams = []
    x = string.split()
    for i in range(len(x)):
        if "".join(sorted(word)) == "".join(sorted(x[i])):
            anagrams.append(x[i])
    return anagrams


print(first_exercise(input("Введите строку: "), input("Введите слово: ")))
