"""
Получить список простых чисел, не превосходящих 100, используя метод решета Эратосфена. 
Метод заключается в исключении из множества натуральных чисел всех чисел, кратных 2 (кроме 2),
кратных 3 (кроме 3) и т.д. Замечание. Число 1 в список простых чисел не входит.
"""


def Eratosthenes(number):
    prime = [True for _ in range(number + 1)]
    p = 2
    while p ** 2 <= number:
        for i in range(p * 2, number + 1, p):
            prime[i] = False
        p += 1
    prime[0] = False
    prime[1] = False
    prime[2] = False
    prime[3] = False
    for p in range(number + 1):
        if prime[p]:
            print(p, end=" ")


Eratosthenes(100)
