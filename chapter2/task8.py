from math import sqrt

"""
Выяснить, существует ли прямоугольник площади S с пери-
метром P. Если существует, то найти его стороны a и b.
"""


def eighth_exercise(square, perimeter):
    if perimeter ** 2 >= 4 * square:
        a = (perimeter - sqrt(perimeter ** 2 - 16 * square)) / 4
        b = (perimeter + sqrt(perimeter ** 2 - 16 * square)) / 4
        return a, b
    return "Прямоугольник не существует."


print(eighth_exercise(float(input("Введите площадь: ")), float(input("Введите периметр: "))))
