from math import sqrt, cos, exp


"""
Вывести таблицу значений функции y = (math.sqrt(math.exp(a * x))) при x <= 0,
y = (math.cos(b * x) / (1 + x ** 2)) при x > 0, если a = 1.6, b = –1.2, а x меняется от –5 до 5 с шагом 0.4. Для кон-
троля вычислить сумму всех полученных значений, результат вывести с тремя знаками после запятой.
"""


def fifth_exercise(a, b, x):
    count = 0
    while x >= -5.0:
        if x <= 0:
            y = sqrt(exp(a * x))
        else:
            y = cos(b * x) / (1 + x ** 2)
        x -= 0.4
        print(f"{x:.3f}: {y:.3f}")
        count += y
    print(f"Контрольное значение: {count:.3f}")


fifth_exercise(1.6, 1.2, 5.4)