from math import *


"""
Ввести с клавиатуры функцию одной переменной x в виде
выражения языка, содержащего x, пределы и шаг изменения пере-
менной. Вывести на экран таблицу значений функции при указанных
значения-дх переменной x с двумя знаками после запятой
"""


def sixth_exercise(y, x, s, expression):
    while x >= y:
        value = eval(expression)
        print(f"{x:.2f}: {value:.2f}")
        x -= s
    return "Программа отработала."


y, x, s = map(float, input("Введите коэффициенты: ").split(","))
expression = input("Введите выражение: ")
print(sixth_exercise(y, x, s, expression))
