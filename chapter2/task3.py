from cmath import cos

"""
Вычислить значение функции y = (a * x ** 3 + b) / (1 + сmath.cos(c * x))
при a = 1+1j; b = 3.7; c = 2; x = –3+2j. Результат вывести с тремя знаками после запятой.
"""


third_exercise = lambda a, b, c, x: (a * x ** 3 + b) / (1 + cos(c * x))
print(f"Значение функции: {third_exercise(1+1j, 3.7, 2, -3+2j):.3f}")
