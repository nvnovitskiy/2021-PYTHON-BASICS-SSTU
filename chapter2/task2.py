from math import sqrt, log, sin, exp

"""
Вычислить значение функции math.sqrt(math.exp(a * x) + x ** 2) * math.log(x ** 2 + b * x + 10)) / (math.sin(c * x) + 4.2
при a=5.7; b=6.4; c=3.1; x=2.8. Результат вывести с двумя знаками после запятой.
"""

second_exercise = lambda x, a, b, c: (sqrt(exp(a * x) + x ** 2) * log(x ** 2 + b * x + 10)) / (sin(c * x) + 4.2)
print(f"Значение функции: {second_exercise(2.8, 5.7, 6.4, 3.1):.2f}")
