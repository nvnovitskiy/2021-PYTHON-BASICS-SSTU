from random import randint

"""
Написать функцию-генератор, которая получает два числовых кортежа и при каждом обращении возвращает 
произведение одного из элементов первого кортежа на элемент второго кортежа (по одному разу каждое произведение). 
Используя эту функцию, вычислить sum^n_{i=1} sum^m_{j=1} a_i b_j.
"""

first_tuple = tuple(randint(0, 50) for _ in range(15))
second_tuple = tuple(randint(0, 50) for _ in range(15))
print(f"Сумма: {sum([i * j for i, j in zip(first_tuple, second_tuple)])}")
