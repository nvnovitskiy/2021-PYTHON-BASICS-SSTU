"""
Написать фабрику функций возведения в n-ную степень.
Создать функции sqrt – для возведения в квадрат и cube – для возведе
ния в куб. Используя эти функции, найти наименьшее натуральное x, такое, 
при котором значение 2x^3 + 17 есть точный квадрат.
"""


def cube(x):
    return x ** 3


def square(y):
    return y ** 2


def expression(x):
    return 2 * cube(x) + 17


def main():
    x, y = 0, 0
    while expression(x) != square(y):
        if expression(x) < square(y):
            x += 1
            expression(x)
        elif expression(x) > square(y):
            y += 1
    print(f"Наименьшее натуральное x: {x}")


main()
