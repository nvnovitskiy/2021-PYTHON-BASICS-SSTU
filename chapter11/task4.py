"""
Добавить в класс «простой прямоугольник» метод, который
получает в качестве параметра другой прямоугольник и возвращает
их объединение, если объединение есть также прямоугольник. Под
объединением прямоугольников следует понимать прикладывание их
сторонами друг к другу без наложения. Если таким способом можно
получить два разных прямоугольника, вернуть оба, если ни одного –
вернуть None.
"""


class SimpleRectangle:
    def __init__(self, height, width):
        self.height = height
        self.width = width

    @property
    def square(self):
        return self.height * self.width

    def CheckingRectangles(self, R):
        height1 = self.height
        width1 = self.width
        height2 = R.height
        width2 = R.width
        if (height1 == height2) & (width1 != width2):
            return height1, width1 + width2, None
        if (height1 != height2) & (width1 == width2):
            return height1 + height2, width2, None
        if (height1 == width2) & (width1 != height2):
            return width1 + height2, width2, None
        if (width1 == height2) & (height1 != width2):
            return height2, height1 + width2, None
        if (height1 == height2) & (width1 == width2):
            return (height1, width1 + width2), (height1 + height2, width1)
        if (height1 == width2) & (width1 == height2):
            return (height2 + width1, width2), (height1, width2 + height1 )
        return None


Rectangle = SimpleRectangle(10, 12)
print(f"Результат: {Rectangle.CheckingRectangles(SimpleRectangle(12, 10))}")
