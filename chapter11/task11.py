"""
Написать программу, которая читает из текстового файла
данные в формате «номер счёта», «вносимая или снимаемая сумма» и
выводит список всех счетов в порядке возрастания номера счёта и величину остатка на счёте.
Вносимая сумма есть положительное целое число, снимаемая сумма есть отрицательное целое число.
"""

with open("task11.txt", "r", encoding="utf-8") as file:
    bank_account = {}
    for line in file.readlines():
        line = line.split()
        account_number, transaction = line[0], int(line[1])
        if account_number not in bank_account:
            bank_account.update({account_number: 0})
        bank_account[account_number] += transaction
    for key, value in sorted(bank_account.items()):
        print(f"Номер счёта: {key}, баланс: {value}.")
