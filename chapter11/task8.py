"""
Добавить в класс «прямоугольник» метод, который возвращает пересечение
текущего объекта и другого прямоугольника – параметра метода или значение None, если прямоугольники не пересекаются.
Задать список прямоугольников и выяснить, имеют ли все прямоугольники общую точку.
Если имеют, то вывести полученное пересечение (оно также является экземпляром класса «прямоугольник»).
"""


class SimpleRectangle:
    def __init__(self, height, width):
        self.height = height
        self.width = width

    @property
    def square(self):
        return self.height * self.width

    def CheckingRectangles(self, Rectangle):
        height1 = self.height, width1 = self.width
        height2 = Rectangle.height, width2 = Rectangle.width
        if (height1 == height2) & (width1 != width2):
            return height1, width1 + width2, None
        if (height1 != height2) & (width1 == width2):
            return height1 + height2, width2, None
        if (height1 == width2) & (width1 != height2):
            return width1 + height2, width2, None
        if (width1 == height2) & (height1 != width2):
            return height2, height1 + width2, None
        if (height1 == height2) & (width1 == width2):
            return (height1, width1 + width2), (height1 + height2, width1)
        if (height1 == width2) & (width1 == height2):
            return (height2 + width1, width2), (height1, width2 + height1)
        return None


class NewRectangle(SimpleRectangle):
    def __init__(self, height, width, x0, y0):
        SimpleRectangle.__init__(self, height, width)
        super().__init__(height, width)
        self.x0 = x0
        self.y0 = y0

    def Belong(self, x, y):
        if (self.x0 < x < self.x0 + self.height) & (self.y0 < y < self.y0 + self.width):
            return True
        return False

    def crossing(self, Rectangle):
        return NewRectangle(min(self.x0 + self.height, Rectangle.x0 + Rectangle.height) - max(self.x0, Rectangle.x0),
                            min(self.y0 + self.width, Rectangle.y0 + Rectangle.width) - max(self.y0, Rectangle.y0),
                            max(self.x0, Rectangle.x0), max(self.y0, Rectangle.y0))

    def print(self):
        print(f"Результат: {self.height, self.width, self.x0, self.y0}")


Rectangles = [NewRectangle(3, 6, 4, 2), NewRectangle(4, 5, 3, 2)]

result = Rectangles[0]
for x in range(0, len(Rectangles) - 1):
    result = result.crossing(Rectangles[x + 1])
if result:
    result.print()
else:
    print(None)

