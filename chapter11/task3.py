"""
Написать класс «простой прямоугольник» со сторонами, параллельными осям координат,
содержащий два числа – размеры сторон. Реализовать вычисление площади прямоугольника как свойство только для чтения.
"""


class SimpleRectangle:
    def __init__(self, height, width):
        self.height = height
        self.width = width

    @property
    def square(self):
        return self.height * self.width


Rectangle = SimpleRectangle(10, 12)
print(f"Результат: {Rectangle.square}")
