"""
Добавить в класс «прямоугольник» метод, который проверяет, принадлежит ли точка,
передаваемая как параметр метода, текущему объекту.
"""


class SimpleRectangle:
    def __init__(self, height, width):
        self.height = height
        self.width = width

    @property
    def square(self):
        return self.height * self.width

    def CheckingRectangles(self, R):
        height1 = self.height, width1 = self.width
        height2 = R.height, width2 = R.width
        if (height1 == height2) & (width1 != width2):
            return height1, width1 + width2, None
        if (height1 != height2) & (width1 == width2):
            return height1 + height2, width2, None
        if (height1 == width2) & (width1 != height2):
            return width1 + height2, width2, None
        if (width1 == height2) & (height1 != width2):
            return height2, height1 + width2, None
        if (height1 == height2) & (width1 == width2):
            return (height1, width1 + width2), (height1 + height2, width1)
        if (height1 == width2) & (width1 == height2):
            return (height2 + width1, width2), (height1, width2 + height1)
        return None


class NewRectangle(SimpleRectangle):
    def __init__(self, height, width, x0=0, y0=0):
        SimpleRectangle.__init__(self, height, width)
        super().__init__(height, width)
        self.x0 = x0
        self.y0 = y0

    def Belong(self, x, y):
        if (self.x0 < x < self.x0 + self.height) & (self.y0 < y < self.y0 + self.width):
            return True
        return False


Checking = NewRectangle(10, 5)
print(f"Результат: {Checking.Belong(4, 3)}")
