"""
Написать класс «отрезок», который содержит координаты
своих вершин. Написать метод, возвращающий точку пересечения
двух отрезков (или None, если отрезки не пересекаются)
"""


class Section:
    def __init__(self, first_line, second_line):
        self.first_line = first_line
        self.second_line = second_line

    def intersection(self):
        x = (self.first_line[0][0] - self.first_line[1][0], self.second_line[0][0] - self.second_line[1][0])
        y = (self.first_line[0][1] - self.first_line[1][1], self.second_line[0][1] - self.second_line[1][1])

        def det(point_a, point_b):
            return point_a[0] * point_b[1] - point_a[1] * point_b[0]

        div = det(x, y)
        if div != 0:
            d = (det(*self.first_line), det(*self.second_line))
            x = det(d, x) / div
            y = det(d, y) / div
            return x, y
        else:
            return None


Lines = Section(((10, 1), (30, 30)), ((21, 16), (21, 6)))
print(f"Результат: {Lines.intersection()}")
