"""
Написать класс, который подсчитывает количество созданных экземпляров этого класса.
"""


class CreatedClasses:
    counter = 0

    def __init__(self):
        self.__class__.counter += 1

    @classmethod
    def CounterCreatedClasses(cls):
        return cls.counter


one = CreatedClasses()
two = CreatedClasses()
three = CreatedClasses()
four = CreatedClasses()

print(f"Созданных экземпляров класса CreatedClasses: {CreatedClasses.CounterCreatedClasses()}")

