from math import hypot, pi, atan2
from random import randint

"""
Написать класс «многоугольник», который содержит координаты его вершин в порядке обхода против часовой стрелки.
Написать методы для вычисления периметра и площади многоугольника
"""


class Polygon:
    def __init__(self, coordinates):
        self.coordinates = coordinates

    def segments(self):
        return zip(self.coordinates, self.coordinates[1:] + [self.coordinates[0]])

    def polygonArea(self):
        return 0.5 * abs(sum(x0 * y1 - x1 * y0 for ((x0, y0), (x1, y1)) in self.segments()))

    def polygonPerimeter(self):
        return abs(sum(hypot(x0 - x1, y0 - y1) for ((x0, y0), (x1, y1)) in self.segments()))


if __name__ == '__main__':
    array = [randint(1, 9) for _ in range(3)]
    Random_Coordinates = [(x, y) for x in array for y in array]
    w = sum(x[0] for x in Random_Coordinates) / len(Random_Coordinates)
    h = sum(x[1] for x in Random_Coordinates) / len(Random_Coordinates)


    def sorting(x):
        return (atan2(x[0] - w, x[1] - h) + 2 * pi) % (2 * pi)


    Random_Coordinates.sort(key=sorting)
    Polygon = Polygon(Random_Coordinates)
    print(f"Площадь многоугольника: {Polygon.polygonArea()}")
    print(f"Периметр многоугольника: {Polygon.polygonPerimeter()}")
