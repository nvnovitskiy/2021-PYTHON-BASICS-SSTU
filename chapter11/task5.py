"""
Заданы три простых прямоугольника. Выяснить, можно ли
приложить их друг к другу без наложений и зазоров так, чтобы получился новый прямоугольник.
Если можно, то вывести размеры получившегося прямоугольника.
"""


class ThreeRectangles:
    def __init__(self, FirstRectangle, SecondRectangle, ThirdRectangle):
        self.FirstRectangle = FirstRectangle
        self.SecondRectangle = SecondRectangle
        self.ThirdRectangle = ThirdRectangle

    def CheckRectangles(self):
        width1, height1 = self.FirstRectangle[0], self.FirstRectangle[1]
        width2, height2 = self.SecondRectangle[0], self.SecondRectangle[1]
        width3, height3 = self.ThirdRectangle[0], self.ThirdRectangle[1]
        if (width1 - width2) * (width1 - height2) * (height1 - width2) * (height1 - height2) * \
                (width2 - width3) * (width2 - height3) * (height2 - width3) * (height2 - height3):
            return None
        else:
            if not width1 - width2:
                height2 += height1
            elif not width1 - height2:
                width2 += height1
            elif not height1 - width2:
                height2 += width1
            elif not height1 - height2:
                width2 += width1
            if not width2 - width3:
                height3 += height2
            elif not width2 - height3:
                width3 += height2
            elif not height2 - width3:
                height3 += width2
            else:
                width3 += width2
            return width3, height3


Rectangles = ThreeRectangles(FirstRectangle=(1, 1), SecondRectangle=(1, 1), ThirdRectangle=(1, 2))
print(f"Результат: {Rectangles.CheckRectangles()}")
