"""
Написать класс, который содержит номер счета, величину
остатка на счёте, и методы для добавления/снятия денег. Другой
класс должен содержать совокупность счетов и уметь отображать их
состояние, причем данные должны выводиться в порядке возрастания
номера счёта.
"""


class Bank:
    def __init__(self):
        self.account_number = 0
        self.transaction = 0
        self.account = {}

    def createAccount(self):
        try:
            self.account_number = input("Введите номер счёта: ")
            if self.account_number not in self.account:
                self.account.update({self.account_number: 0})
            if self.account_number == "":
                raise ValueError
        except ValueError:
            print("Пустой ввод. Программа завершается.")
            exit()

    def Transaction(self):
        try:
            self.transaction = int(input("Введите транзакцию: "))
            self.account[self.account_number] += self.transaction
        except:
            print("Пустой ввод. Программа завершается.")
            exit()


class OutputBankAccount(Bank):
    def __init__(self):
        super().__init__()

    def printAccount(self):
        for key, value in sorted(self.account.items()):
            print(f"Номер счёта: {key}, баланс: {value}.")


if __name__ == '__main__':
    print("Добро пожаловать в программу 'Банковский счет'!")
    new = OutputBankAccount()
    new.createAccount()
    new.Transaction()
    new.printAccount()

    while True:
        operation = input("Хотите провести ещё одну операцию? (да/нет): ")
        if operation != "да":
            print("Завершение программы. До свидания!")
            exit()
        new.createAccount()
        new.Transaction()
        new.printAccount()
