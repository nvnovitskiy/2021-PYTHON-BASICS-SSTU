from random import uniform, choice

"""
Карьер отгружает организации песок, гравий и щебень грузовыми машинами. Каждая машина взвешивается до и после погрузки,
и эти данные записываются в журнал в формате «вид груза», «вес машины до погрузки», «вес машины после погрузки».
Создайте текстовый файл, содержащий 20 случайных строк в указанном формате. 
Прочтите файл и определите, сколько всего песка, сколько всего гравия и сколько всего щебня было отгружено организации.
Указание: Для случайного выбора вида груза при создании файла удобно использовать функцию choice() из модуля random.
"""

cargo = ["Песок", "Гравий", "Щебень"]

with open("task4.txt", "w", encoding="utf-8") as file:
    for _ in range(20):
        before_loading = uniform(6, 30)
        after_loading = uniform(before_loading, 40)
        file.writelines(choice(cargo) + " " + str(before_loading) + " " + str(after_loading) + "\n")

sand_counter = 0
gravel_counter = 0
rubble_counter = 0

with open("task4.txt", "r", encoding="utf-8") as file:
    for string in file.readlines():
        string = string.split()
        if string[0] == "Песок":
            sand_counter += float(string[2]) - float(string[1])
        elif string[0] == "Гравий":
            gravel_counter += float(string[2]) - float(string[1])
        elif string[0] == "Щебень":
            rubble_counter += float(string[2]) - float(string[1])

print(f"Песка было отгружено: {sand_counter}")
print(f"Гравия было отгружено: {gravel_counter}")
print(f"Щебня было отгружено: {rubble_counter}")
