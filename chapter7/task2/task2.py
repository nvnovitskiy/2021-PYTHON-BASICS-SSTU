from re import findall

"""
В текстовом файле приведен текст стихотворения
С.Я. Маршака «Дом, который построил Джек». Подсчитать частоту
появления каждого слова, считая строчные и прописные буквы неразличимыми. 
Учесть, что знак препинания не является частью слова.
"""

with open("task2.txt", "r", encoding="utf-8") as file:
    frequency = {}
    reading = file.read().lower()
    pattern = findall(r"\b[а-я]{1,30}\b", reading)

    for word in pattern:
        count = frequency.get(word, 0)
        frequency[word] = count + 1

    for words in frequency.keys():
        print(f"{words}: {frequency[words]}")
