# Основы программирования на языке Python (Самарский Государственный Технический Университет)

Версия Python: 3.9.7
____

# Методическое пособие: 
+ [Ссылка на методическое пособие с лабораторными работами](http://pm.samgtu.ru/sites/pm.samgtu.ru/files/materials/oop/python3.1.pdf)
____

# Лабораторные работы:

 № Л/р | Название | Статус| Ссылка
 ----- |----------|-------|------
 1 | Краткий обзор языка | ✅| [Ссылка](https://github.com/nvnovitskiy/python-for-beginners-sstu/tree/main/chapter1)
 2 | Числовые типы | ✅ | [Ссылка](https://github.com/nvnovitskiy/python-for-beginners-sstu/tree/main/chapter2)
 3 | Символы и строки | ✅ | [Ссылка](https://github.com/nvnovitskiy/python-for-beginners-sstu/tree/main/chapter3)
 4 | Списки и кортежи | ✅ | [Ссылка](https://github.com/nvnovitskiy/python-for-beginners-sstu/tree/main/chapter4)
 5 | Словари | ✅ | [Ссылка](https://github.com/nvnovitskiy/python-for-beginners-sstu/tree/main/chapter5)
 6 | Множества | ✅ | [Ссылка](https://github.com/nvnovitskiy/python-for-beginners-sstu/tree/main/chapter6) 
 7 | Файлы | ✅ | [Ссылка](https://github.com/nvnovitskiy/python-for-beginners-sstu/tree/main/chapter7)
 8 | Процедурное программирование | ✅ | [Ссылка](https://github.com/nvnovitskiy/python-for-beginners-sstu/tree/main/chapter8) 
 9 | Модули | ✅ | [Ссылка](https://github.com/nvnovitskiy/python-for-beginners-sstu/tree/main/chapter9)  
10 | Функциональное программирование | ✅ | [Ссылка](https://github.com/nvnovitskiy/python-for-beginners-sstu/tree/main/chapter10)  
11 | Объектно-ориентированное программирование | ✅ | [Ссылка](https://github.com/nvnovitskiy/python-for-beginners-sstu/tree/main/chapter11) 
