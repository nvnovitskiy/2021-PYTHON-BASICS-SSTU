"""
Ввести текстовую строку. Сформировать новую строку, в которой сначала идут символы исходной строки с четными индексами
(0, 2, 4,…), а затем – с нечетными индексами (1, 3, 5, …).
"""


string = input("Введите строку: ")
print(f"Результат: {string[::2] + string[1::2]}")
