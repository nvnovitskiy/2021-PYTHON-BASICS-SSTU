"""
Сколько раз встречается каждая из цифр 0, 1, ... , 9 в числе 2^N? (N вводится с клавиатуры). 
"""


def ninth_exercise(power):
    print(f"Число возведенное в степень: {2 ** power}")
    for i in range(0, 10):
        print(f"{i}: {str(2 ** power).count(str(i))}")


ninth_exercise(int(input("Введите степень: ")))