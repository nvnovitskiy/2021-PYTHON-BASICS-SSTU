"""
Сколько нулей и сколько единиц потребуется, чтобы записать все числа от 1 до N в двоичной системе счисления (N задано в
десятичной системе счисления)?
"""


def fourteenth_exercise(number):
    unit, zero = 0, 0
    for i in range(1, number + 1):
        string = str(bin(i))
        string = str(string[2:len(string)])
        for j in range(len(string)):
            if string[j] == "1":
                unit += 1
            else:
                zero += 1
    return unit, zero


print(fourteenth_exercise(int(input("Введите число: "))))
