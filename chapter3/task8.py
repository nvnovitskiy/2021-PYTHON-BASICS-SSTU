"""
Ввести текстовую строку, состоящую только из десятичных
цифр. Определить, сколько раз каждый из символов 0 , ... , 9 встречается в строке.
"""


def eighth_exercise(string):
    for i in range(0, 10):
        print(f"{i}: {string.count(str(i))}")


eighth_exercise(input("Введите строку: "))