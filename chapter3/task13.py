"""
Ввести текстовую строку и создать новую «зашифрованную» строку, в которой каждая буква (символ кириллицы) циклически 
сдвигается по алфавиту на k позиций «вправо» отдельно для прописных, отдельно для строчных букв.
Символы, отличные от букв, остаются без изменения.
"""


def thirteenth_exercise(string, shift):
    result = ""
    for i in range(len(string)):
        if 1040 <= ord(string[i]) <= 1103 or ord(string[i]) == 1025 or ord(string[i]) == 1105 or ord(
                string[i]) == 1037 or ord(string[i]) == 1117:
            result += chr(ord(string[i]) + shift)
        else:
            result += string[i]
    return result


string = input("Введите строку: ")
shift = int(input("Введите сдвиг: "))
print(thirteenth_exercise(string, shift))
