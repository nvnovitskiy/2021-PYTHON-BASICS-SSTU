"""
Переставить первые k символов введённого слова в его конец (значение k вводится с клавиатуры, k меньше длины слова). 
Пример. Введено слово «камыш» и k=2, тогда после перестановки получим слово «мышка»
"""


tenth_exercise = lambda string, shift: string[len(string) - shift - 1:len(string)] + string[0:shift]
string = input("Введите строку: ")
shift = int(input("Введите коэффициент сдвига: "))
print(tenth_exercise(string, shift))
