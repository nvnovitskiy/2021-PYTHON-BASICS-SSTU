from random import sample, randint

"""
Матрица называется разрежённой, если только относительно
небольшое количество её элементов отлично от 0. В этом случае
можно хранить только её ненулевые элементы с указанием номера
строки и номера столбца, на пересечении которых расположен данный элемент. 
Используя тип «словарь», задать две разрежённые матрицы и вычислить их сумму.
"""


def sum_of_two_arrays(rows, columns):
    first_array_to_dict = {i: {j: randint(1, 10) for j in sample(range(0, rows), randint(1, rows))} for i in
                           sample(range(0, columns), randint(1, columns))}
    second_array_to_dict = {i: {j: randint(1, 10) for j in sample(range(0, rows), randint(1, rows))} for i in
                            sample(range(0, columns), randint(1, columns))}
    sum_of_arrays = dict()

    def print_array(array, columns, rows):
        for i in range(columns):
            if i in array:
                for j in range(rows):
                    if j in array[i]:
                        print(array[i][j], end=" ")
                    else:
                        print(0, end=" ")
                print()
            else:
                print(*[0 for i in range(rows)])
        print()

    print("Первый массив:")
    print_array(first_array_to_dict, columns, rows)
    print("Второй массив:")
    print_array(second_array_to_dict, columns, rows)

    for i in range(columns):
        count1, count2 = 0, 0
        for j in range(rows):
            if i in first_array_to_dict:
                count1 = first_array_to_dict[i].get(j, 0)
            if i in second_array_to_dict:
                count2 = second_array_to_dict[i].get(j, 0)
            if count1 + count2:
                sum_of_arrays[i] = sum_of_arrays.get(i, {})
                sum_of_arrays[i][j] = sum_of_arrays[i].get(j, 0) + count1 + count2

    print("Сумма массивов:")
    print_array(sum_of_arrays, columns, rows)
    return "Программа отработала"


rows, columns = map(int, input("Введите количество строк и столбцов: ").split())
sum_of_two_arrays(rows, columns)
