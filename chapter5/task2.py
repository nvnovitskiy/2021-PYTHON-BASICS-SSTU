"""
Написать программу, определяющую тип выигрышной комбинации при игре в покер. 
Программа получает на вход 5 целых положительных чисел, и должна вывести на экран одну из следующих строк:
«poker», если все 5 чисел равны;
«four of a kind», если ровно 4 числа равны между собой;
«full house», если три из пяти чисел равны между собой и два оставшихся числа равны;
«three of a kind», если ровно три числа равны;
«two pairs», если есть две пары равных чисел;
«one pair», если только два числа равны;
«all different», если все числа различны.
"""


def second_exercise(cards):
    cards = {card: cards.count(card) for card in cards}.values()
    if len(cards) == 1:
        return "Poker"
    elif len(cards) == 2:
        return "Full House" if 2 in cards else "Four of a kind"
    elif len(cards) == 3:
        return "Three of a kind" if 3 in cards else "Two pairs"
    elif len(cards) == 4:
        return "One pair"
    else:
        return "All different"


cards = [int(input()) for _ in range(5)]
print(f"Cards: {cards}")
print(f"Result: {second_exercise(cards)}")
