"""
Ввести данные о банковских операциях на счетах клиентов:
номер счета и сумма прихода (положительное число) или расхода
(отрицательное число). Получить список счетов клиентов и итоговую сумму на каждом
счете (предполагается, что исходно на всех счетах нулевые значения).
Результат (по возрастанию номера счета) вывести на экран.
"""


def fifth_exercise():
    bank_account = dict()
    while True:
        account_number = input("Введите номер счёта: ")
        if account_number not in bank_account:
            bank_account.update({account_number: 0})
        if account_number == "":
            print("Пустой ввод. Программа завершается.")
            break
        transaction = int(input("Введите транзакцию: "))
        bank_account[account_number] += transaction
        for key, value in sorted(bank_account.items()):
            print(f"Номер счёта: {key}, баланс: {value}.")


fifth_exercise()
