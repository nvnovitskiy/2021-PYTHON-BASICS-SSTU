"""
Получить последовательность из 40 чисел Фибоначчи, если
они определяются следующим образом: f(n) = {1, если n=1 или 2; f(n-1)+f(n-2), если n >= 3}.
"""


def seventh_exercise():
    fibonacci = {1: 1, 2: 1}
    for number in range(3, 41):
        fibonacci[number] = fibonacci[number - 1] + fibonacci[number - 2]
    for key, value in fibonacci.items():
        print(f"{key}: {value}")


seventh_exercise()
