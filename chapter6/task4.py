"""
Во входной строке записана последовательность чисел через пробел. Для каждого числа
выведите слово YES (в отдельной строке), если это число ранее встречалось в
последовательности или NO, если не встречалось
"""

lst = list(map(int, input("Введите числа: ").split()))
set_of_numbers = set()

for item in lst:
    if item in set_of_numbers:
        print("YES")
    else:
        print("NO")
        set_of_numbers.add(item)