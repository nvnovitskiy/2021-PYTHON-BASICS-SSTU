import itertools

"""
Получить множество натуральных чисел, не превосходящих 200,
которые делятся на 16 и представимы в виде 15n+23m, где n, m – натуральные числа. Результат вывести в упорядоченном по возрастанию виде.
"""


# Решение задачи через функцию
def third_exercise():
    array = set()
    for n, m in itertools.product(range(1, 10), range(1, 10)):
        x = 15 * n + 23 * m
        if x % 16 == 0 and x <= 200:
            array.add(x)
    print(sorted(array))


third_exercise()

# Решение задачи в одну строку
print(sorted(set((15 * n + 23 * m) for n, m in itertools.product(range(1, 10), range(1, 10)) if
                 (15 * n + 23 * m) % 16 == 0 and (15 * n + 23 * m) <= 200)))
