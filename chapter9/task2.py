"""
Используя цифры 1, 2, 3, 4, 5, 6, 7, 8, 9 по одному разу, составить два натуральных числа А и В, 
таких, что A корень из B. Найти все решения.
"""


def second_exercise():
    for y in range(1, 32000):
        x = y ** 2
        str_a, str_b = str(x), str(y),
        set_a, set_b = set(str_a), set(str_b)
        if '0' not in set_a and '0' not in set_b:
            if len(set_a) == len(str_a) and len(set_b) == len(str_b):
                print(f"Корень из {x} = {y}")


second_exercise()
