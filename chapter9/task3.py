from itertools import combinations

"""
Расставить в кружки числа от 1 до 9 так, чтобы на всех сторонах треугольника были одинаковыми (рисунок в методичке):
а) суммы чисел;
б) суммы их квадратов.
Вывести все варианты расстановки (числа выводятся в порядке нумерации кружков).
"""


def third_exercise():
    print("Расстановки, которые удовлевторяют условиям: ")
    for i in combinations(range(1, 9), 4):
        for j in combinations(range(1, 9), 3):
            for k in combinations(range(1, 9), 2):
                if sum(i) == i[3] + sum(j) == j[2] + sum(k) + i[0]:
                    if sum(i) ** 2 == (i[3] + sum(j)) ** 2 == (j[2] + sum(k) + i[0]) ** 2:
                        print(i + j + k)


third_exercise()
