from random import randint

"""
Найти наименьшее натуральное число, записываемое в десятичной системе счисления только с помощью цифр 0 и 1, 
которое делится без остатка на заданное число К (2 ≤ К ≤ 100).
"""


def first_exercise(numbers, divider):
    array = [number for number in numbers if number % divider == 0]

    if len(array) != 0:
        return min(array)
    else:
        return "Список пустой!"


numbers = [int("".join([str(randint(0, 1)) for _ in range(0, 10)])) for _ in range(100)]
divider = int(input("Введите делитель: "))
print(first_exercise(numbers, divider))
