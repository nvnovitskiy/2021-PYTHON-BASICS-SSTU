"""
Известна стоимость N предметов, которые необходимо разделить на K (K<N) групп так,
чтобы максимальная разница суммарной стоимости предметов разных групп была минимальной.
Пример. Пусть стоимости предметов равны 320, 110, 50, 200 и 70
рублей, и K = 2. Тогда решение задачи имеет вид: Группа 1: 320 + 50 = 370; Группа 2: 110 + 200 + 70 = 380.
"""

subjects = [320, 110, 50, 200, 70]
first_group, second_group = [], []
maximum_difference = sum(subjects)

for i in range(2 ** len(subjects)):
    first_array, second_array = [], []
    for x, y in zip(format(i, f"0{len(subjects):d}b"), subjects):
        if x == '0':
            first_array.append(y)
        else:
            second_array.append(y)
    difference = abs(sum(first_array) - sum(second_array))
    if maximum_difference > difference:
        first_group, second_group, maximum_difference = first_array, second_array, difference

print(f"Группа I: {first_group}")
print(f"Группа II: {second_group}")
print(f"Разница между группой I и группой II: {maximum_difference}")
