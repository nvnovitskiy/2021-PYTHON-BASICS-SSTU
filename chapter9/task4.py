from itertools import permutations

"""
Найти все решения арифметических ребусов:
А) трава + корова + доярка = молоко
Б) один + один = много
В) два * два = четыре
Г) RE + MI = FA
   DO + SI = MI
   LA + SI = SOL
"""


# Решение для пункта А

def first_point(word):
    counter = 0
    for char in word:
        counter = counter * 10 + dictionary[char]
    return counter


string = set("травакоровадояркамолоко")

for x in permutations(range(10), len(string)):
    dictionary = dict(zip(string, x))
    a = first_point("трава")
    b = first_point("корова")
    c = first_point("доярка")
    d = first_point("молоко")
    if a > 9999 and b > 99999 and c > 99999 and 99999 < d == a + b + c:
        print(f"Решением ребуса для пункта А является: {a} + {b} + {c} = {d}")


# Решение для пункта Б

def second_point(word):
    count = 0
    for char in word:
        count = count * 10 + dictionary[char]
    return count


string = set("одинодинмного")

for x in permutations(range(10), len(string)):
    dictionary = dict(zip(string, x))
    a = second_point("один")
    b = second_point("один")
    c = second_point("много")
    if a > 999 and b > 999 and 9999 < c == a + b:
        print(f"Решением ребуса для пункта Б является: {a} + {b} = {c}")


# Решение для пунка В

def third_point(word):
    count = 0
    for char in word:
        count = count * 10 + dictionary[char]
    return count


string = set("одинодинмного")

for x in permutations(range(10), len(string)):
    dictionary = dict(zip(string, x))
    a = third_point("один")
    b = third_point("один")
    c = third_point("много")
    if a > 999 and b > 999 and 9999 < c == a + b:
        print(f"Решением ребуса для пункта В является: {a} + {b} = {c}")


# Решение для пунка Г

def fourth_point(word):
    count = 0
    for char in word:
        count = count * 10 + dictionary[char]
    return count


string = set("LASISOL")

for x in permutations(range(10), len(string)):
    dictionary = dict(zip(string, x))
    a = fourth_point("LA")
    b = fourth_point("SI")
    c = fourth_point("SOL")
    if a > 9 and b > 9 and 99 < c == a + b:
        print(f"Решением ребуса для пункта Г является: {a} + {b} = {c}")
