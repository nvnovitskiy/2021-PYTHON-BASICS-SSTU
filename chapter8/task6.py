from math import *

"""
Написать функцию, которая получает функцию f, значение x и целое положительное число n, 
и возвращает значение f(f(…f (x)…)) – результат n вложенных вызовов функции f.
"""


def expression(x):
    return sin(x)


def sixth_exercise(f, x, calls):
    expression = f(x)
    print(expression)
    while not calls == 0:
        return sixth_exercise(f, expression, calls - 1)


if __name__ == '__main__':
    x = float(input("Введите аргумент x: "))
    calls = int(input("Введите число вызовов функции: "))
    sixth_exercise(expression, x, calls)
