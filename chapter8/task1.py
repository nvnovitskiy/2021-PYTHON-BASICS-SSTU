from math import atan2
from random import randint

"""
Заполнить случайными целыми числами список кортежей (x, y) – точек плоскости, заданных своими декартовыми координатами. 
Список отсортировать по возрастанию полярного угла точки, результат вывести на экран.
"""

array = [randint(1, 9) for _ in range(3)]
random_tuple = [(x, y) for x in array for y in array]
print(f"Случайный список кортежей: {random_tuple}")
print(f"Отсортированные кортежи по полярному углу точки: {sorted(random_tuple, key=lambda x: atan2(x[1], x[0]))}")
