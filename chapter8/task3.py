from math import sqrt
from random import randint

"""
Написать функцию, которая получает список целых положительных чисел и возвращает список тех значений из полученного
списка, которые являются квадратом некоторого натурального числа.
"""

third_exercise = list(map(lambda x: x, filter(lambda x: sqrt(x).is_integer(), [randint(1, 100) for _ in range(50)])))
print(f"Результат: {third_exercise}")

