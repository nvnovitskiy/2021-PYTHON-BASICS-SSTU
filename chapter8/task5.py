from random import uniform

"""
Написать функцию, которая получает функцию f и список неповторяющихся значений её аргумента и возвращает словарь вида
x : f(x). Предусмотреть два варианта вызова функции: с фиксированным и переменным числом параметров.
"""


def fifth_exercise(values, expression):
    result = {}
    for x in values:
        result.update({round(x, 2): round(eval(expression), 2)})
    return result


expression = input("Введите выражение: ")
values = [uniform(0, 10) for _ in range(7)]
print(f"Результат: {fifth_exercise(values, expression)}")
