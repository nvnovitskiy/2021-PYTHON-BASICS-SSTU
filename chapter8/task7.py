from random import randint

"""
Написать функцию, которая получает в качестве параметров 
некоторую функцию f и переменное число вещественных чисел xi и возвращает сумму f(xi).
"""


def seventh_exercise(expression, values):
    count = 0
    for x in values:
        count += eval(expression)
    return count


if __name__ == '__main__':
    expression = input("Введите функцию: ")
    values = [randint(0, 100) for _ in range(50)]
    print(f"Результат: {seventh_exercise(expression, values)}")
