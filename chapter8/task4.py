from collections import Counter

"""
Написать функцию, которая получает строки А и В и возвращает True, если все символы строки А (с учетом повторений) содержатся в строке В.
Примеры: Для А = 'интерес', В = 'строение' функция должна вернуть True, а для А = 'интерес', В = 'иностранец' – значение False, так как в этом
случае в А содержатся две буквы 'е', а в В – только одна, хотя все остальные символы присутствуют в нужном количестве.
"""


def fourth_exercise(string1, string2):
    string1, string2 = Counter(string1), Counter(string2)
    for key in string1.keys():
        if string1[key] > string2[key]:
            return False
    return True


string1, string2 = map(str, input("Введите две строки: ").split())
print(f"Результат: {fourth_exercise(string1, string2)}")
