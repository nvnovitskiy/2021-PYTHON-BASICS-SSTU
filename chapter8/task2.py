from random import uniform

"""
Написать функцию, которая получает аргумент x и список коэффициентов многочлена и возвращает значение многочлена в точке x.
Предусмотреть два варианта вызова функции: с фиксированным и переменным числом параметров.
"""


def second_exercise(expression, coefficients):
    for x in coefficients:
        print(f"{x:.3f}: {eval(expression):.3f}")


coefficients = [uniform(0, 35) for _ in range(10)]
expression = input("Введите выражение: ")
second_exercise(expression, coefficients)
