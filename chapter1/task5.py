"""
Сколько отрицательных значений встретится в бесконечной
последовательности x_0 = -100; x_k = 1 + x_k-1 / k, k = 1, 2, 3...?
"""


def fifth_exercise(x, counter):
    for i in range(1, 10):
        if x < 0:
            x = 1 + x / i
            counter += 1
    print(f"Отрицательных чисел в последовательности: {counter}")
    
    
fifth_exercise(-100, 0)

