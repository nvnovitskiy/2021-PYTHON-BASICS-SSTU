"""
Из одинаковых кубиков с ребром 1 см выкладывают пирами-
ду, k-тый слой которой (считая сверху) состоит из k2 кубиков. Какой
высоты получится пирамида, если в наличии имеется N кубиков?
Сколько останется неиспользованных кубиков?
Получить результат для N = 15; 30; 155; 5555.
"""


def sixth_exercise(number_of_cubes):
    i, counter = 0, 0
    while number_of_cubes > 0:
        number_of_cubes -= i ** 2
        if number_of_cubes >= 0:
            counter = number_of_cubes
        if number_of_cubes < 0:
            break
        i += 1
    print(f"Высота пирамиды: {i - 1}, неиспользованных кубиков: {counter}")


sixth_exercise(int(input("Введите количество кубиков: ")))
