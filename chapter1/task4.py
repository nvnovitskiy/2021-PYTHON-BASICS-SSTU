"""
100 гномов едят одно яблоко весом 200 грамм. Каждый гном
откусывает одну сотую того, что ему досталось и ещё один грамм.
Вычислить вес огрызка.
"""

fourth_exercise = lambda weight, remainder, gnomes: weight * (remainder ** gnomes) - (1 - remainder ** gnomes) / (
            1 - remainder)
print(f"Вес огрызка: {fourth_exercise(200, 0.99, 100):.2f} грамм")
